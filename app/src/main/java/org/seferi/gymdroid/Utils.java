package org.seferi.gymdroid;

import java.util.ArrayList;
import java.util.UUID;

public class Utils {
    private static ArrayList<Training> trainings;
    private static ArrayList<Plan> plans;

    public static void initTrainings() {
        final String contactId = UUID.randomUUID().toString();
        if (null == trainings) {
            trainings = new ArrayList<>();
        }

            Training pushUp = new Training(contactId, "Push up", "raising and lowering the body with the straightening and bending of the arms while keeping the back straight and supporting the body on the hands and toes",
                    "Gravity’s pull on your own body weight provides the resistance needed to develop your chest, shoulder and triceps. Place your hands on the floor so they’re slightly outside shoulder-width. Spread your fingers slightly out and have them pointed forward. Raise up onto your toes so that all of your body weight is on your hands and your feet. Contract your abdominals to keep your torso in a straight line and prevent arching your back or pointing your bottom in the air. Bend your elbows and lower your chest down toward the floor. Once your elbows bend slightly beyond 90 degrees, push off the floor and extend them so that you return to starting position.",
                    "https://i2.pickpik.com/photos/478/253/195/academy-bodybuilding-flexion-illustration-preview.jpg");

            trainings.add(pushUp);

    }

    public static ArrayList<Training> getTrainings() {
        return trainings;
    }

    public static boolean addPlan(Plan plan) {
        if (null == plans) {
            plans = new ArrayList<>();
        }

        return plans.add(plan);
    }

    public static ArrayList<Plan> getPlans() {
        return plans;
    }

    public static boolean removePlan(Plan plan) {
        return plans.remove(plan);
    }
}

